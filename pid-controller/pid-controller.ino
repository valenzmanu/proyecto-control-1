// Ejemplo_Timer
// Código original tomado de:
//      https://henryforceblog.wordpress.com/2015/05/02/blink-example-using-timer-on-a-tiva-launchpad/
// Modificado por Luis Alberto Rivera

#include "wiring_private.h"
#include "inc/hw_ints.h"
#include "driverlib/interrupt.h"
#include "driverlib/rom.h"
#include "driverlib/timer.h"
#include "driverlib/sysctl.h"

const int analogInPin = A3;
const int analogInPin2= A0;
int dig_pin=0;
int val_in=0;
int val_in_map=0;
int val_out=0;
int myPins[] = {34,33,32,15,14,13,12,11};// LSB-MSB
int myVals[] = {0,0,0,0,0,0,0,0,0};
volatile uint8_t state = 0;


// Constantes del Controlador PID //
const float kp=10.4361;
const float ki=2.1741*0.0001;
const float kd=0.027707/0.0001;

// Variable de controlador //
float u_k=0;
//  VAriables del error. //
float eD=0;
float e_k=0;
float E_k=0;
float e_k_1=0;
float r=0;
float y=0;
void setup(){
  // put your setup code here, to run once: 
  configureTimer1A(); // llamado a configuración del timer.
  //Serial.begin(9600); 
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  pinMode(14, OUTPUT);
  pinMode(15, OUTPUT);
  pinMode(32, OUTPUT);
  pinMode(33, OUTPUT);
  pinMode(34, OUTPUT);
  pinMode(A0, INPUT);
  pinMode(A3,INPUT);
}

void loop(){
  // El loop principal no hace nada. Todo se realiza en el handler de la interrupción, cuando
  // se vence el timer.
}

// Función que configura el timer (1A en este ejemplo)
void configureTimer1A(){
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1); // Enable Timer 1 Clock
  ROM_IntMasterEnable(); // Enable Interrupts
  ROM_TimerConfigure(TIMER1_BASE, TIMER_CFG_PERIODIC); // Configure Timer Operation as Periodic
  
  // Configure Timer Frequency
  // Frequency is given by MasterClock / CustomValue
  // Examples: 80MHz / 80k = 1000 Hz ; 80MHz / 80M = 1 Hz
  ROM_TimerLoadSet(TIMER1_BASE, TIMER_A, 8000); 

  // Al parecer, no hay función ROM_TimerIntRegister definida. Usar la de memoria FLASH
  // El prototipo de la función es:
  //    extern void TimerIntRegister(uint32_t ui32Base, uint32_t ui32Timer, void (*pfnHandler)(void));
  // Con el tercer argumento se especifica el handler de la interrupción (puntero a la función).
  // Usar esta función evita tener que hacer los cambios a los archivos internos de Energia,
  // sugeridos en la página de donde se tomó el código original.
  TimerIntRegister(TIMER1_BASE, TIMER_A, &Timer1AHandler);
  
  ROM_IntEnable(INT_TIMER1A);  // Enable Timer 1A Interrupt
  ROM_TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT); // Timer 1A Interrupt when Timeout
  ROM_TimerEnable(TIMER1_BASE, TIMER_A); // Start Timer 1A
}


// Handler (ISR) de la interrupción del Timer
// AQUI ESTA EL CODIO DE LA INTERRUPCION //
void Timer1AHandler(void){
  //Required to launch next interrupt
  ROM_TimerIntClear(TIMER1_BASE, TIMER_A);
  //Iniciamos aqui....
  // Leemos el valor que entra a la TIVA//
  r = analogRead(analogInPin);    //A3
  y = analogRead(analogInPin2);   //A0

  //Mapeo [0,4095] -> [0,3.3]
  r = (r/4095.0)*3.3;
  y = (y/4095.0)*3.3;
  
  // Se implemnta el PID//
  //////////////////////////////////////
    e_k=r-y;
    eD=e_k - e_k_1;
    E_k = E_k + e_k;
    u_k=kp*e_k+ki*E_k+kd*eD;
    e_k_1=e_k;
    

    if (u_k>3){
      u_k=3;
    }
    if (u_k<-3){
      u_k=-3;
    }
    
   //////////////////////////////////////
  // Se mapea el valor...//  
  //(255/(cs-ci))*(u-ci))
  val_in_map = (int)((255/6)*(u_k+3));
  //val_in_map = (int)(u_k*255.0/4095.0);  
  //val_in_map = (int)(r*255.0/3.3);
  for(int i=0; i<8;i++)
   {
    myVals[i]=bitRead(val_in_map,i);
   }  
   for(int j=0; j<8;j++)
   {
      digitalWrite(myPins[j],myVals[j]);
      //Serial.print(myVals[j]);
   }
   //Serial.println();
}
